import pygame
import random
import os
from pygame.constants import QUIT, K_DOWN, K_UP, K_LEFT, K_RIGHT

pygame.init()

BOARD_HEIGHT = 800
BOARD_WIDTH = 1200
BOARD_COLOR = (229, 204, 255)
BOARD_FONT = pygame.font.SysFont('Verdana', 20)
BOARD_BLACK = (0, 0, 0)
BOARD_BG = pygame.transform.scale(pygame.image.load(
    'background.png'), (BOARD_WIDTH, BOARD_HEIGHT))
bg_X1 = 0
bg_X2 = BOARD_BG.get_width()
bg_move = 3

PLAYER_COLOR = (153, 51, 255)
PLAYER_SIZE = (30, 20)

ENEMY_SIZE = (30, 20)
ENEMY_COLOR = (0, 0, 255)

BONUS_SIZE = (20, 30)
BONUS_COLOR = (50, 205, 50)

FPS = pygame.time.Clock()

main_display = pygame.display.set_mode((BOARD_WIDTH, BOARD_HEIGHT))
main_display.fill(BOARD_COLOR)

IMAGE_PATH = "Goose"
PLAYER_IMAGES = os.listdir(IMAGE_PATH)

# player = pygame.Surface(PLAYER_SIZE)
player = pygame.image.load('player.png').convert_alpha()
# player.fill(PLAYER_COLOR)
player_rect = pygame.Rect(
    0, BOARD_HEIGHT/2, *PLAYER_SIZE)
player_move_down = [0, 4]
player_move_right = [4, 0]
player_move_up = [0, -4]
player_move_left = [-4, 0]
# player_speed = [1, 1]


def create_enemy():
    # enemy = pygame.Surface(ENEMY_SIZE)
    # enemy.fill(ENEMY_COLOR)
    enemy = pygame.image.load('enemy.png').convert_alpha()
    enemy_rect = pygame.Rect(
        BOARD_WIDTH, random.randint(100, BOARD_HEIGHT-100), *ENEMY_SIZE)
    enemy_move = [random.randint(-8, -1), 0]
    return [enemy, enemy_rect, enemy_move]


CREATE_ENEMY = pygame.USEREVENT + 1
pygame.time.set_timer(CREATE_ENEMY, 1500)


def create_bonus():
    # bonus = pygame.Surface(BONUS_SIZE)
    # bonus.fill(BONUS_COLOR)
    bonus = pygame.image.load('bonus.png').convert_alpha()
    bonus_rect = pygame.Rect(
        random.randint(100, BOARD_WIDTH), 0, *BONUS_SIZE)
    bonus_move = [0, random.randint(4, 8)]
    return [bonus, bonus_rect, bonus_move]


CREATE_BONUS = pygame.USEREVENT + 2
pygame.time.set_timer(CREATE_BONUS, 800)
CHANGE_IMAGE = pygame.USEREVENT + 3
pygame.time.set_timer(CHANGE_IMAGE, 200)

enemies = []
bonuses = []
score = 0
image_index = 0
playing = True

while playing:
    FPS.tick(120)
    for event in pygame.event.get():
        if event.type == QUIT:
            playing = False
        if event.type == CREATE_ENEMY:
            enemies.append(create_enemy())
        if event.type == CREATE_BONUS:
            bonuses.append(create_bonus())
        if event.type == CHANGE_IMAGE:
            player = pygame.image.load(
                os.path.join(IMAGE_PATH, PLAYER_IMAGES[image_index]))
            image_index += 1
        if image_index >= len(PLAYER_IMAGES):
            image_index = 0

    # main_display.fill(BOARD_COLOR)
    bg_X1 -= bg_move
    bg_X2 -= bg_move
    if bg_X1 < -BOARD_BG.get_width():
        bg_X1 = BOARD_BG.get_width()
    if bg_X2 < -BOARD_BG.get_width():
        bg_X2 = BOARD_BG.get_width()
    main_display.blit(BOARD_BG, (bg_X1, 0))
    main_display.blit(BOARD_BG, (bg_X2, 0))

    keys = pygame.key.get_pressed()

    if keys[K_DOWN] and player_rect.bottom < BOARD_HEIGHT:
        player_rect = player_rect.move(player_move_down)
    if keys[K_RIGHT] and player_rect.right < BOARD_WIDTH:
        player_rect = player_rect.move(player_move_right)
    if keys[K_UP] and player_rect.top > 0:
        player_rect = player_rect.move(player_move_up)
    if keys[K_LEFT] and player_rect.left > 0:
        player_rect = player_rect.move(player_move_left)

    for enemy in enemies:
        enemy[1] = enemy[1].move(enemy[2])
        main_display.blit(enemy[0], enemy[1])
        if player_rect.colliderect(enemy[1]):
            playing = False

    for bonus in bonuses:
        bonus[1] = bonus[1].move(bonus[2])
        main_display.blit(bonus[0], bonus[1])
        if player_rect.colliderect(bonus[1]):
            score += 1
            bonuses.pop(bonuses.index(bonus))

    main_display.blit(BOARD_FONT.render(str(score), True,
                      BOARD_BLACK), (BOARD_WIDTH - 50, 20))
    main_display.blit(player, player_rect)

    # print(len(bonuses))
    pygame.display.flip()

    for enemy in enemies:
        if enemy[1].left < 0:
            enemies.pop(enemies.index(enemy))

    for bonus in bonuses:
        if bonus[1].bottom > BOARD_HEIGHT:
            bonuses.pop(bonuses.index(bonus))
# random move
    # if player_rect.bottom >= BOARD_HEIGHT:
    #     player_speed = random.choice(([2, -1], [-1, -1]))
    # if player_rect.right >= BOARD_WIDTH:
    #     player_speed = random.choice(([-2, -1], [-1, 1]))
    # if player_rect.top <= 0:
    #     player_speed = random.choice(([-2, 1], [1, 1]))
    # if player_rect.left <= 0:
    #     player_speed = random.choice(([1, 2], [1, -1]))
